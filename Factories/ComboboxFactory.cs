﻿using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Models;
using OntoWebCore.Attributes;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Factories
{
    public class ComboboxFactory<TTYPE>
    {

        private clsLogStates logStates = new clsLogStates();
        private JsonFactory jsonFactory = new JsonFactory();

        public JqxDataSource CreateDataSource(List<object> itemList, SessionFile sessionFile)
        {
            var type = typeof(TTYPE);
            var result = jsonFactory.CreateJsonFileOfItemList(type, (List<object>)itemList, sessionFile);

            if (result.GUID == logStates.LogState_Error.GUID)
            {
                return null;
            }

            string idProperty = "Id";

            var propattList = type.GetProperties().Cast<PropertyInfo>().Select(propItem =>
            {
                var attributeItem = propItem.GetCustomAttribute<ComboboxAttribute>();

                if (attributeItem == null) return null;

                if (attributeItem.IsIdField)
                {
                    idProperty = propItem.Name;
                }
                return new { Property = propItem, Attribute = attributeItem };
            }).Where(propAtt => propAtt != null).ToList().Select(propAtt =>
            {
                DataField dataField;
                if (propAtt.Property.PropertyType == typeof(bool))
                {
                    dataField = new DataField(DataFieldType.Bool)
                    {
                        name = !string.IsNullOrEmpty(propAtt.Attribute.Property) ? propAtt.Attribute.Property : propAtt.Property.Name
                    };
                }
                else if (propAtt.Property.PropertyType == typeof(int))
                {
                    dataField = new DataField(DataFieldType.Int)
                    {
                        name = !string.IsNullOrEmpty(propAtt.Attribute.Property) ? propAtt.Attribute.Property : propAtt.Property.Name
                    };
                }
                else if (propAtt.Property.PropertyType == typeof(long))
                {
                    dataField = new DataField(DataFieldType.Number)
                    {
                        name = !string.IsNullOrEmpty(propAtt.Attribute.Property) ? propAtt.Attribute.Property : propAtt.Property.Name
                    };
                }
                else if (propAtt.Property.PropertyType == typeof(double))
                {
                    dataField = new DataField(DataFieldType.Float)
                    {
                        name = !string.IsNullOrEmpty(propAtt.Attribute.Property) ? propAtt.Attribute.Property : propAtt.Property.Name
                    };
                }
                else if (propAtt.Property.PropertyType == typeof(DateTime))
                {
                    dataField = new DataField(DataFieldType.Date)
                    {
                        name = !string.IsNullOrEmpty(propAtt.Attribute.Property) ? propAtt.Attribute.Property : propAtt.Property.Name
                    };
                }
                else
                {
                    dataField = new DataField(DataFieldType.String)
                    {
                        name = !string.IsNullOrEmpty(propAtt.Attribute.Property) ? propAtt.Attribute.Property : propAtt.Property.Name
                    };
                }

                return dataField;
            }).ToList();

            var jqxDataSource = new JqxDataSource(JsonDataType.Json)
            {
                datafields = propattList,
                id = idProperty,
                url = sessionFile.FileUri.AbsoluteUri
            };

            return jqxDataSource;
        }
    }
}
