﻿using Newtonsoft.Json;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using OntoWebCore.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Factories
{
    public class JsonFactory
    {
        private clsLogStates logStates = new clsLogStates();
        private object entryItem;
        private string currentNameSpace;

        public clsOntologyItem CreateJsonFileOfItemList(Type itemType, List<object> itemList, SessionFile sessionFile)
        {
            var result = logStates.LogState_Success.Clone();
            var entryNamespace = itemType.Namespace;
            currentNameSpace = itemType.Namespace;
            var propAttributes = itemType.GetProperties().Cast<PropertyInfo>().Select(prop =>
            {
                var jsonAttribute = (JsonAttribute)prop.GetCustomAttribute(typeof(JsonAttribute));

                return new { Property = prop, JsonAttribute = jsonAttribute };
            }).ToList();

            if (!propAttributes.Any()) return logStates.LogState_Error.Clone();


            using (sessionFile.StreamWriter)
            {
                using (var jsonStream = new Newtonsoft.Json.JsonTextWriter(sessionFile.StreamWriter))
                {
                    jsonStream.WriteStartArray();
                    itemList.ForEach(item =>
                    {
                        jsonStream.WriteStartObject();

                        propAttributes.ForEach(propAtt =>
                        {
                            bool isList = propAtt.Property.PropertyType.IsGenericType
                            && propAtt.Property.PropertyType.GetGenericTypeDefinition() == typeof(IList<>);

                            if (isList)
                            {
                                var value = propAtt.Property.GetValue(item);
                                var collection = (IEnumerable)value;
                                foreach (var colItem in collection)
                                {
                                    result = WriteItemToJson(colItem, jsonStream, entryNamespace);
                                }
                            }
                            else
                            {
                                var value = propAtt.Property.GetValue(item);

                                if (value != null && propAtt.JsonAttribute != null)
                                {






                                    var propertyName = propAtt.Property.Name;
                                    if (!string.IsNullOrEmpty(propAtt.JsonAttribute.Property))
                                    {
                                        propertyName = propAtt.JsonAttribute.Property;
                                    }

                                    if (propAtt.JsonAttribute.PropertyNameToLowercase)
                                    {
                                        propertyName = propertyName.ToLower();

                                    }

                                    if (propAtt.JsonAttribute.FirstCharLowercase && propertyName.Length > 0)
                                    {
                                        propertyName = propertyName.Substring(0, 1).ToLower() + propertyName.Substring(1);
                                    }

                                    jsonStream.WritePropertyName(propertyName);

                                    if (value.GetType().IsEnum)
                                    {
                                        var enumName = Enum.GetName(value.GetType(), value);
                                        var enumValue = (int)value;

                                        if (propAtt.JsonAttribute.EnumToString)
                                        {
                                            var propertyValue = enumName;

                                            if (propAtt.JsonAttribute.EnumStringToLowercase)
                                            {
                                                propertyValue = propertyValue.ToLower();
                                            }


                                            if (propAtt.JsonAttribute.EnumStringFirstCharToLowercase)
                                            {
                                                propertyValue = propertyValue.Substring(0, 1).ToLower() + propertyValue.Substring(1);
                                            }

                                            jsonStream.WriteValue(propertyValue);

                                        }
                                        else
                                        {
                                            jsonStream.WriteValue(enumValue);
                                        }



                                    }
                                    else
                                    {
                                        var valueNameSpace = value.GetType().Namespace;
                                        if (valueNameSpace.StartsWith(entryNamespace) ||
                                            valueNameSpace.StartsWith(currentNameSpace))
                                        {
                                            result = WriteItemToJson(value, jsonStream, entryNamespace);
                                        }
                                        else
                                        {
                                            jsonStream.WriteValue(value);
                                        }

                                    }











                                }
                            }
                        });

                        jsonStream.WriteEndObject();
                    });
                    jsonStream.WriteEndArray();
                }
            }

            return result;
        }

        public clsOntologyItem CreateJsonFileOfJsonItem(object item, SessionFile sessionFile)
        {
            
            var result = logStates.LogState_Success.Clone();
            entryItem = item;
            var entryNamespace = item.GetType().Namespace;

            try
            {
                using (sessionFile.StreamWriter)
                {
                    using (var jsonWriter = new JsonTextWriter(sessionFile.StreamWriter))
                    {
                        result = WriteItemToJson(item, jsonWriter, entryNamespace);
                    }
                }
            }
            catch (Exception ex)
            {
                result = logStates.LogState_Success.Clone();
            }
                       

            return result;
        }

        public clsOntologyItem WriteItemToJson(object item, JsonTextWriter jsonTextWriter, string entryNamespace)
        {
            var result = logStates.LogState_Success.Clone();
            currentNameSpace = item.GetType().Namespace;
            //try
            //{
            var propAttributes = item.GetType().GetProperties().Cast<PropertyInfo>().Select(prop =>
                {
                    var jsonAttribute = (JsonAttribute)prop.GetCustomAttribute(typeof(JsonAttribute));

                    return new { Property = prop, JsonAttribute = jsonAttribute };
                }).ToList();

                if (!propAttributes.Any()) return logStates.LogState_Error.Clone();

                jsonTextWriter.WriteStartObject();
                propAttributes.ForEach(propAtt =>
                {
                    
                    bool isList = propAtt.Property.PropertyType.IsGenericType
                            && propAtt.Property.PropertyType.GetGenericTypeDefinition() == typeof(IList<>);

                    if (isList)
                    {
                        var value = propAtt.Property.GetValue(item);
                        var collection = (IEnumerable)value;
                        foreach (var colItem in collection)
                        {
                            result = WriteItemToJson(colItem, jsonTextWriter, entryNamespace);
                        }
                    }
                    else
                    {
                        var value = propAtt.Property.GetValue(item);

                        if (value != null && propAtt.JsonAttribute != null)
                        {

                            

                            


                            var propertyName = propAtt.Property.Name;
                            if (!string.IsNullOrEmpty(propAtt.JsonAttribute.Property))
                            {
                                propertyName = propAtt.JsonAttribute.Property;
                            }

                            if (propAtt.JsonAttribute.PropertyNameToLowercase)
                            {
                                propertyName = propertyName.ToLower();

                            }

                            if (propAtt.JsonAttribute.FirstCharLowercase && propertyName.Length > 0)
                            {
                                propertyName = propertyName.Substring(0, 1).ToLower() + propertyName.Substring(1);
                            }

                            jsonTextWriter.WritePropertyName(propertyName);

                            if (value.GetType().IsEnum)
                            {
                                var enumName = Enum.GetName(value.GetType(), value);
                                var enumValue = (int)value;

                                if (propAtt.JsonAttribute.EnumToString)
                                {
                                    var propertyValue = enumName;
                                    
                                    if (propAtt.JsonAttribute.EnumStringToLowercase)
                                    {
                                        propertyValue = propertyValue.ToLower();
                                    }


                                    if (propAtt.JsonAttribute.EnumStringFirstCharToLowercase)
                                    {
                                        propertyValue = propertyValue.Substring(0, 1).ToLower() + propertyValue.Substring(1);
                                    }
                                        
                                    jsonTextWriter.WriteValue(propertyValue);

                                }
                                else
                                {
                                    jsonTextWriter.WriteValue(enumValue);
                                }

                                

                            }
                            else
                            {
                                var valueNameSpace = value.GetType().Namespace;
                                if (valueNameSpace.StartsWith(entryNamespace) ||
                                    valueNameSpace.StartsWith(currentNameSpace))
                                {
                                    result = WriteItemToJson(value, jsonTextWriter, entryNamespace);
                                }
                                else
                                {
                                    jsonTextWriter.WriteValue(value);
                                }
                                
                            }


                            

                            



                            

                            
                        }
                    }
                    
                });

                jsonTextWriter.WriteEndObject();
            //}
            //catch (Exception ex)
            //{
            //    result = globals.LState_Error.Clone();
            //}
            

            return result;
        }

        public JsonFactory()
        {
            
        }
    }
}
