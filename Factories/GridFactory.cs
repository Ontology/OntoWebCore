﻿using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Models;
using OntoWebCore.Attributes;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Factories
{
    public static class GridFactory
    {
        private static clsLogStates logStates = new clsLogStates();

        public static ViewItem CreateViewItem(List<object> ontologyViewItems, Type typeItem, string viewItemId, string viewItemClass, string viewItemType)
        {
            var propAtts = typeItem.GetProperties().Cast<PropertyInfo>().Select(prop =>
            {
                var attribute = prop.GetCustomAttribute<DataViewColumnAttribute>();

                if (attribute == null)
                {
                    return null;
                }

                return new { Prop = prop, Attrib = attribute };
            }).Where(propAtt => propAtt != null).ToList();

            var viewItem = new ViewItem
            {
                ViewItemId = viewItemId,
                ViewItemClass = viewItemClass,
                ViewItemType = viewItemType
            };


            var dictList = new List<Dictionary<string, object>>();
            ontologyViewItems.ForEach(oItem =>
            {
                var dict = new Dictionary<string, object>();
                propAtts.ForEach(propAtt =>
                {
                    dict.Add(propAtt.Prop.Name, propAtt.Prop.GetValue(oItem));
                });

                dictList.Add(dict);
            });

            viewItem.ViewItemValue = dictList;

            return viewItem;
        }

        public static async Task<clsOntologyItem> WriteKendoJson(Type modelType, List<object> itemList, SessionFile sessionFile)
        {

            var properties = modelType.GetProperties().Cast<PropertyInfo>().Select(propItem =>
            {
                var attributeItem = propItem.GetCustomAttribute<KendoColumnAttribute>();

                if (attributeItem == null) return null;


                return new { Property = propItem, Attribute = attributeItem };
            }).Where(propAtt => propAtt != null).ToList();

            using (sessionFile.StreamWriter)
            {
                using (var jsonTextWriter = new Newtonsoft.Json.JsonTextWriter(sessionFile.StreamWriter))
                {
                    jsonTextWriter.WriteStartArray();

                    itemList.ForEach(itm =>
                    {
                        jsonTextWriter.WriteStartObject();

                        properties.ForEach(propAtt =>
                        {
                            jsonTextWriter.WritePropertyName(propAtt.Property.Name);
                            jsonTextWriter.WriteValue(propAtt.Property.GetValue(itm));
                        });

                        jsonTextWriter.WriteEndObject();
                    });

                    jsonTextWriter.WriteEndArray();
                }
                    
            }

            return logStates.LogState_Success.Clone();
        }

        public static Dictionary<string, object> CreateKendoGridConfig(Type modelType,
            SessionFile sessionFile)
        {
            var gridConfig = modelType.GetCustomAttribute<KendoGridConfigAttribute>();
            var pageable = modelType.GetCustomAttribute<KendoPageableAttribute>();
            var filter = modelType.GetCustomAttribute<KendoFilterAttribute>();
            var stringFilter = modelType.GetCustomAttribute<KendoStringFilterableAttribute>();
            var numberFilter = modelType.GetCustomAttribute<KendoNumberFilterableAttribute>();

            

            var dataSource = new KendoDatasource
            {
                pageSize = 20,
                transport = new KendoTransport
                {
                    read = new KendoTransportRead
                    {
                        dataType = "json",
                        url = sessionFile.FileUri.AbsoluteUri
                    }
                }
            };

            var dictGridConfig = new Dictionary<string, object>();
            dictGridConfig.Add("dataSource", dataSource);
            if (gridConfig != null)
            {
                dictGridConfig.Add(nameof(KendoGridConfigAttribute.height), gridConfig.height);
                dictGridConfig.Add(nameof(KendoGridConfigAttribute.width), gridConfig.width);
                dictGridConfig.Add(nameof(KendoGridConfigAttribute.groupbable), gridConfig.groupbable);
                dictGridConfig.Add(nameof(KendoGridConfigAttribute.sortable), gridConfig.sortable);
                dictGridConfig.Add(nameof(KendoGridConfigAttribute.autoBind), gridConfig.autoBind);
                if (!string.IsNullOrEmpty(gridConfig.selectable))
                {
                    if (gridConfig.selectable.ToLower() == "true")
                    {
                        dictGridConfig.Add(nameof(KendoGridConfigAttribute.selectable), true );
                    }
                    else
                    {
                        dictGridConfig.Add(nameof(KendoGridConfigAttribute.selectable), gridConfig.selectable);
                    }
                }
                
            }

            var pagableDict = new Dictionary<string, object>();

            if (pageable != null)
            {
                pagableDict.Add(nameof(KendoPageableAttribute.refresh), pageable.refresh);
                pagableDict.Add(nameof(KendoPageableAttribute.pageSizes), pageable.pageSizes);
                pagableDict.Add(nameof(KendoPageableAttribute.buttonCount), pageable.buttonCount);
                pagableDict.Add(nameof(KendoPageableAttribute.pageSize), pageable.pageSize);
                dictGridConfig.Add("pageable", pagableDict);
            }

            
            var dictColumns = new List<Dictionary<string, object>>();
            modelType.GetProperties().Cast<PropertyInfo>().Select(propItem =>
            {
                var attributeItem = propItem.GetCustomAttribute<KendoColumnAttribute>();

                if (attributeItem == null) return null;


                return new { Property = propItem, Attribute = attributeItem };
            }).Where(propAtt => propAtt != null && !propAtt.Attribute.hidden).OrderBy(propAtt => propAtt.Attribute.Order).ToList().ForEach(propAtt =>
            {
                var dictColumn = new Dictionary<string, object>();
                dictColumn.Add(nameof(KendoColumnAttribute.field), propAtt.Property.Name);
                if (propAtt.Attribute.width != null)
                {
                    dictColumn.Add(nameof(KendoColumnAttribute.width), propAtt.Attribute.width);
                }

                dictColumn.Add(nameof(KendoColumnAttribute.filterable), propAtt.Attribute.filterable);
                dictColumn.Add(nameof(KendoColumnAttribute.title), !string.IsNullOrEmpty(propAtt.Attribute.title) ? propAtt.Attribute.title : propAtt.Property.Name);
                dictColumn.Add(nameof(KendoColumnAttribute.Order), propAtt.Attribute.Order);
                if (!string.IsNullOrEmpty(propAtt.Attribute.format))
                {
                    dictColumn.Add(nameof(KendoColumnAttribute.format), propAtt.Attribute.format);
                }
                dictColumn.Add(nameof(KendoColumnAttribute.editable), propAtt.Attribute.editable);
                dictColumns.Add(dictColumn);
                
            });

            dictGridConfig.Add("columns", dictColumns);
            if (!string.IsNullOrEmpty(gridConfig.rowTemplate))
            {
                dictGridConfig.Add("rowTemplate", gridConfig.rowTemplate);
            }
            
            

            if (filter != null || 
                stringFilter != null ||
                numberFilter != null)
            {
                var dictFilterable = new Dictionary<string, object>();

                if (filter != null)
                {
                    dictFilterable.Add(nameof(KendoFilterAttribute.extra), filter.extra);
                }

                var dictOperators = new Dictionary<string, object>();
                if (stringFilter != null)
                {
                    var dictFilter = new Dictionary<string, object>();
                    dictFilter.Add(nameof(KendoStringFilterableAttribute.contains), stringFilter.contains);
                    dictFilter.Add(nameof(KendoStringFilterableAttribute.startsWith), stringFilter.startsWith);
                    dictFilter.Add(nameof(KendoStringFilterableAttribute.eq), stringFilter.eq);
                    dictFilter.Add(nameof(KendoStringFilterableAttribute.neq), stringFilter.neq);
                    dictFilter.Add(nameof(KendoStringFilterableAttribute.isempty), stringFilter.isempty);
                    dictOperators.Add("string", dictFilter);
                }

                if (numberFilter != null)
                {
                    var dictFilter = new Dictionary<string, object>();
                    dictFilter.Add(nameof(KendoStringFilterableAttribute.contains), stringFilter.contains);
                    dictFilter.Add(nameof(KendoStringFilterableAttribute.startsWith), stringFilter.startsWith);
                    dictFilter.Add(nameof(KendoStringFilterableAttribute.eq), stringFilter.eq);
                    dictFilter.Add(nameof(KendoStringFilterableAttribute.neq), stringFilter.neq);
                    dictOperators.Add("number", dictFilter);
                }

                dictFilterable.Add("operators", dictOperators);

                dictGridConfig.Add("filterable", dictFilterable);
            }

            dictGridConfig.Add("dataBound", null);
            dictGridConfig.Add("change", null);
            if (!string.IsNullOrEmpty(gridConfig.editable))
            {
                dictGridConfig.Add("editable", gridConfig.editable);
            }
            

            return dictGridConfig;
        }

        public static JqxDataSource CreateJqxDataSource(Type modelType, int? pageNum, int? pageSize, string fileUrl)
        {
            string idProperty = "";
            var propattList = modelType.GetProperties().Cast<PropertyInfo>().Select(propItem =>
            {
                var attributeItem = propItem.GetCustomAttribute<DataViewColumnAttribute>();

                if (attributeItem == null) return null;

                if (attributeItem.IsIdField)
                {
                    idProperty = propItem.Name;
                }
                return new { Property = propItem, Attribute = attributeItem };
            }).Where(propAtt => propAtt != null).ToList().Select(propAtt =>
            {
                DataField dataField;
                if (propAtt.Property.PropertyType == typeof(bool))
                {
                    dataField = new DataField(DataFieldType.Bool)
                    {
                        name = propAtt.Property.Name
                    };
                }
                else if (propAtt.Property.PropertyType == typeof(int) ||
                    propAtt.Property.PropertyType == typeof(int?))
                {
                    dataField = new DataField(DataFieldType.Int)
                    {
                        name = propAtt.Property.Name
                    };
                }
                else if (propAtt.Property.PropertyType == typeof(long) ||
                    propAtt.Property.PropertyType == typeof(long?))
                {
                    dataField = new DataField(DataFieldType.Number)
                    {
                        name = propAtt.Property.Name
                    };
                }
                else if (propAtt.Property.PropertyType == typeof(double) ||
                    propAtt.Property.PropertyType == typeof(double?))
                {
                    dataField = new DataField(DataFieldType.Float)
                    {
                        name = propAtt.Property.Name
                    };
                }
                else if (propAtt.Property.PropertyType == typeof(DateTime) ||
                    propAtt.Property.PropertyType == typeof(DateTime?))
                {
                    dataField = new DataField(DataFieldType.Date)
                    {
                        name = propAtt.Property.Name
                    };
                }
                else
                {
                    dataField = new DataField(DataFieldType.String)
                    {
                        name = propAtt.Property.Name
                    };
                }

                return dataField;
            }).ToList();

            var jqxDataSource = new JqxDataSource(JsonDataType.Json)
            {
                datafields = propattList,
                id = idProperty,
                url = fileUrl,
                pagenum = pageNum ?? 0,
                pagesize = pageSize ?? 20
            };

            return jqxDataSource;
        }
        public static JqxColumnList CreateColumnList(Type modelType)
        {
            var jqxColumnList = new JqxColumnList();
            jqxColumnList.ColumnList = modelType.GetProperties().Cast<PropertyInfo>().Select(propItem =>
            {
                var attributeItem = propItem.GetCustomAttribute<DataViewColumnAttribute>();

                if (attributeItem == null) return null;

                JqxColumnAttribute jqxColumn;

                if (!string.IsNullOrEmpty(attributeItem.CellsFormat))
                {
                    jqxColumn = new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.None)
                    {
                        datafield = propItem.Name,
                        text = attributeItem.Caption,
                        width = 100,
                        hidden = !attributeItem.IsVisible,
                        editable = !attributeItem.IsReadonly,
                        cellsrenderer = attributeItem.StandardRenderer == StandardRenderer.Generic ? null : attributeItem.StandardRenderer.ToString(),
                        cellsformat = attributeItem.CellsFormat

                    };
                }
                else
                {
                    if (propItem.PropertyType == typeof(bool))
                    {
                        jqxColumn = new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Checkbox)
                        {
                            datafield = propItem.Name,
                            text = attributeItem.Caption,
                            width = 100,
                            hidden = !attributeItem.IsVisible,
                            editable = !attributeItem.IsReadonly,
                            cellsrenderer = attributeItem.StandardRenderer == StandardRenderer.Generic ? null : attributeItem.StandardRenderer.ToString()

                        };
                    }
                    else
                    {
                        jqxColumn = new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Textbox)
                        {
                            datafield = propItem.Name,
                            text = attributeItem.Caption,
                            width = 100,
                            hidden = !attributeItem.IsVisible,
                            editable = !attributeItem.IsReadonly,
                            cellsrenderer = attributeItem.StandardRenderer == StandardRenderer.Generic ? null : attributeItem.StandardRenderer.ToString()
                        };
                    }
                }
                    


                return jqxColumn;
            }).Where(propAtt => propAtt != null).ToList().ToList();

            return jqxColumnList;
        }
    }
}
