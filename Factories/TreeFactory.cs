﻿using OntoWebCore.Attributes;
using Newtonsoft.Json;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OntoWebCore.Factories
{
    public class TreeFactory: NotifyPropertyChange
    {
        private Globals globals;
        private Thread writeJsonAsync;

        private object factoryLocker = new object();

        private FactoryResultTreeListConfig resultTreeListConfig;
        public FactoryResultTreeListConfig ResultTreeListConfig
        {
            get
            {
                lock(factoryLocker)
                {
                    return resultTreeListConfig;
                }
            }
            set
            {
                lock(factoryLocker)
                {
                    resultTreeListConfig = value;
                }
                RaisePropertyChanged(nameof(ResultTreeListConfig));
            }
        }

        private clsOntologyItem resultJson;
        public clsOntologyItem ResultJson
        {
            get { return resultJson; }
            set
            {
                resultJson = value;
                RaisePropertyChanged(OntoMsg_Module.Notifications.NotifyChanges.TreeFactory_ResultJson);
            }
        }

        public string NotifyResult
        {
            get { return OntoMsg_Module.Notifications.NotifyChanges.TreeFactory_ResultJson; }
        }

        private List<ViewTreeNode> treeNodes;
        private SessionFile sessionFile;

        public TreeFactory(Globals globals)
        {
            this.globals = globals;
        }

        public clsOntologyItem WriteJson(List<ViewTreeNode> treeNodes, SessionFile sessionFile)
        {
            this.treeNodes = treeNodes;
            this.sessionFile = sessionFile;

            StopFactory();

            writeJsonAsync = new Thread(WriteJsonAsync);
            writeJsonAsync.Start();

            return globals.LState_Success.Clone();

        }

        private void WriteJsonAsync()
        {
            using (sessionFile.StreamWriter)
            {
                using (var jsonWriter = new JsonTextWriter(sessionFile.StreamWriter))
                {
                    treeNodes.OrderBy(treeNode => treeNode.Name).ToList().ForEach(treeNode =>
                    {
                        jsonWriter.WriteStartArray();

                        treeNode.WriteJSON(jsonWriter);                 

                        jsonWriter.WriteEndArray();
                    });
                }
                    
            }

            ResultJson = globals.LState_Success.Clone();
                
        }

        
        public async Task<FactoryResultTreeListConfig> CreateKendoTreeListConfig(Type modelType, SessionFile sessionFile)
        {
            var treeListConfigAttribute = modelType.GetCustomAttribute<KendoTreeListConfigAttribute>();


            var resultItem = new FactoryResultTreeListConfig
            {
                Result = globals.LState_Success.Clone()
            };

            var transport = new KendoTransport
            {
                read = new KendoTransportRead
                {
                    dataType = "json",
                    url = sessionFile.FileUri.AbsoluteUri
                }
            };

            var dictTreeListConfig = new Dictionary<string, object>();
            if (treeListConfigAttribute != null)
            {
                if (!string.IsNullOrEmpty(treeListConfigAttribute.width))
                {
                    dictTreeListConfig.Add(nameof(KendoTreeListConfigAttribute.width), treeListConfigAttribute.width);
                }
                if (!string.IsNullOrEmpty(treeListConfigAttribute.height))
                {
                    dictTreeListConfig.Add(nameof(KendoTreeListConfigAttribute.height), treeListConfigAttribute.height);
                }
                dictTreeListConfig.Add(nameof(KendoTreeListConfigAttribute.sortable), treeListConfigAttribute.sortable);
                dictTreeListConfig.Add(nameof(KendoTreeListConfigAttribute.filterable), treeListConfigAttribute.filterable);

            }
            var dictDataSource = new Dictionary<string, object>();
            dictTreeListConfig.Add("dataSource", dictDataSource);

            dictDataSource.Add("transport", transport);

            var properties = modelType.GetProperties().Cast<PropertyInfo>().ToList();

            var propAtts = properties.Select(prop =>
            {
                var dataSourceField = prop.GetCustomAttribute<KendoDataSourceFieldAttribute>();
                var kendoColumn = prop.GetCustomAttribute<KendoTreeListColumnAttribute>();

                return new { Prop = prop, DataSourceField = dataSourceField, Column = kendoColumn };
            }).ToList();

            var idField = propAtts.FirstOrDefault(propAtt => propAtt.DataSourceField.IsIdField);
            var idParentField = propAtts.FirstOrDefault(propAtt => propAtt.DataSourceField.IsParentIdField);
            var restFields = propAtts.Where(propAtt => !propAtt.DataSourceField.IsIdField && !propAtt.DataSourceField.IsParentIdField).ToList();
            var columns = propAtts.Where(propAtt => propAtt.Column != null).OrderBy(propAtt => propAtt.Column.Order).ToList();

            if (idField == null || idParentField == null)
            {
                resultItem.Result = globals.LState_Error.Clone();
                ResultTreeListConfig = resultItem;
                return resultItem;
            }

            var dictSchema = new Dictionary<string, object>();
            var dictModel = new Dictionary<string, object>();

            if (treeListConfigAttribute != null)
            {
                dictModel.Add(nameof(KendoTreeListConfigAttribute.expanded), treeListConfigAttribute.expanded);
            }

            dictSchema.Add("model", dictModel);
            dictDataSource.Add("schema", dictSchema);

            //dictModel.Add("id", idField.Prop.Name);
            dictModel.Add("id", "id");
            //dictModel.Add("parentId", idParentField.Prop.Name);


            var dictFields = new Dictionary<string, object>();
            dictModel.Add("fields", dictFields);

            

            restFields.ForEach(field =>
            {
                var dictField = new Dictionary<string, object>();
                if (!string.IsNullOrEmpty(field.DataSourceField.field))
                {
                    dictField.Add(nameof(KendoDataSourceFieldAttribute.field), field.DataSourceField.field);
                }
                
                if (!string.IsNullOrEmpty(field.DataSourceField.type))
                {
                    dictField.Add(nameof(KendoDataSourceFieldAttribute.type), field.DataSourceField.type);
                }

                dictField.Add(nameof(KendoDataSourceFieldAttribute.nullable), field.DataSourceField.nullable);
                dictFields.Add(field.Prop.Name, dictField);

                
                
            });


            var dictColumns = new List<Dictionary<string, object>>();
            dictTreeListConfig.Add("columns", dictColumns);

            columns.ForEach(column =>
            {
                var dictColumn = new Dictionary<string, object>();
                dictColumn.Add(nameof(KendoColumnAttribute.field), column.Prop.Name);
                if (column.Column.width != null)
                {
                    dictColumn.Add(nameof(KendoColumnAttribute.width), column.Column.width);
                }

                dictColumn.Add(nameof(KendoColumnAttribute.filterable), column.Column.filterable);
                dictColumn.Add(nameof(KendoColumnAttribute.title), !string.IsNullOrEmpty(column.Column.title) ? column.Column.title : column.Prop.Name);

                if (!string.IsNullOrEmpty(column.Column.format))
                {
                    dictColumn.Add(nameof(KendoColumnAttribute.format), column.Column.format);
                }
                dictColumn.Add(nameof(KendoColumnAttribute.editable), column.Column.editable);
                dictColumn.Add(nameof(KendoTreeListColumnAttribute.expandable), column.Column.expandable);
                dictColumns.Add(dictColumn);
            });

            resultItem.TreeListConfig = dictTreeListConfig;
            ResultTreeListConfig = resultItem;
            return resultItem;


        }

        public void StopFactory()
        {
            if (writeJsonAsync != null)
            {
                try
                {
                    writeJsonAsync.Abort();
                }
                catch (Exception ex)
                {

                }
            }
        }
    }

    public class FactoryResultTreeListConfig
    {
        public clsOntologyItem Result { get; set; }
        public Dictionary<string, object> TreeListConfig { get; set; }
    }
}
