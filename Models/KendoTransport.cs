﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Models
{
    public class KendoTransport
    {
        public KendoTransportRead read { get; set; }
    }

    public class KendoTransportRead
    {
        public string url { get; set; }
        public string dataType { get; set; }
    }
}
