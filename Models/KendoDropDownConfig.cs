﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Models
{
    public class KendoDropDownConfig
    {
        public string optionLabel { get; set; }
        public string dataTextField
        {
            get;
            private set;
        }
        public string dataValueField
        {
            get;
            private set;
        }
        public List<KendoDropDownItem> dataSource { get; set; }

        public KendoDropDownConfig()
        {
            dataTextField = nameof(KendoDropDownItem.Text);
            dataValueField = nameof(KendoDropDownItem.Value);
        }
    }

    public class KendoDropDownItem
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}
