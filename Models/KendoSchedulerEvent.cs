﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Models
{
    public class KendoSchedulerEvent
    {
        public string title { get; set; }
        public object taskId { get; set; }
        public DateTime start { get; set; }
        public string startTimezone { get; set; }
        public DateTime end { get; set; }
        public string endTimezone { get; set; }
        public string recurrenceRule { get; set; }
        public string recurrenceException { get; set; }
        public bool isAllDay { get; set; }
        public string description { get; set; }
        public object ownerId { get; set; }
        public string uid { get; set; }
    }
}
