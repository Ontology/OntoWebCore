﻿using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Models
{
    public class OFilterItem : ViewModelBase
    {
        public string NotifyIdObjectItem
        {
            get { return "IdObjectItem"; }
        }

        public string NotifyNameObjectItem
        {
            get { return "NameObjectItem"; }
        }

        public string NotifyIdClassItem
        {
            get { return "IdClassItem"; }
        }

        public string NotifyNameClassItem
        {
            get { return "NameClassItem"; }
        }

        public string NotifyIdRelationTypeItem
        {
            get { return "IdRelationTypeItem"; }
        }

        public string NotifyNameRelationTypeItem
        {
            get { return "NameRelationTypeItem"; }
        }

        public string NotifyIdDirectionItem
        {
            get { return "IdDirectionItem"; }
        }

        public string NotifyNameDirectionItem
        {
            get { return "NameDirectionItem"; }
        }

        public string NotifyFilterNullItem
        {
            get
            {
                return "FilterNullItem";
            }
        }

        public string NotifyIsDirectionLeftRight
        {
            get
            {
                return "IsDirectionLeftRight";
            }
        }

        public string NotifyIsEnabledAddObject
        {
            get
            {
                return "IsEnabledAddObject";
            }
        }

        public string NotifyIsEnabledRemoveObject
        {
            get
            {
                return "IsEnabledRemoveObject";
            }
        }

        public string NotifyIsEnabledNullObject
        {
            get
            {
                return "IsEnabledNullObject";
            }
        }


        private string idObjectItem;
        [ViewModel(Send = true)]
        public string IdObjectItem
        {
            get { return idObjectItem; }
            set
            {
                idObjectItem = value;
                ValidateButtons();
                RaisePropertyChanged(NotifyIdObjectItem);
            }
        }

        private string nameObjectItem;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpObject", ViewItemType = ViewItemType.Content)]
        public string NameObjectItem
        {
            get { return nameObjectItem; }
            set
            {
                nameObjectItem = value;
                RaisePropertyChanged(NotifyNameObjectItem);
            }
        }

        private string idClassItem;
        [ViewModel(Send = true)]
        public string IdClassItem
        {
            get { return idClassItem; }
            set
            {
                idClassItem = value;
                ValidateButtons();
                RaisePropertyChanged(NotifyIdClassItem);
            }
        }

        private string nameClassItem;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpClass", ViewItemType = ViewItemType.Content)]
        public string NameClassItem
        {
            get { return nameClassItem; }
            set
            {
                nameClassItem = value;
                RaisePropertyChanged(NotifyNameClassItem);
            }
        }

        private string idRelationTypeItem;
        [ViewModel(Send = true)]
        public string IdRelationTypeItem
        {
            get { return idRelationTypeItem; }
            set
            {
                idRelationTypeItem = value;
                RaisePropertyChanged(NotifyIdRelationTypeItem);
            }
        }

        private string nameRelationTypeItem;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpRelationType", ViewItemType = ViewItemType.Content)]
        public string NameRelationTypeItem
        {
            get { return nameRelationTypeItem; }
            set
            {
                nameRelationTypeItem = value;
                ValidateButtons();
                RaisePropertyChanged(NotifyNameRelationTypeItem);
            }
        }

        private string idDirectionItem;
        [ViewModel(Send = true)]
        public string IdDirectionItem
        {
            get { return idDirectionItem; }
            set
            {
                idDirectionItem = value;
                if (idDirectionItem == "cc99d5365d564fd29d4f45b48af33029")
                {
                    IsDirectionLeftRight = true;
                }
                else
                {
                    IsDirectionLeftRight = false;
                }
                RaisePropertyChanged(NotifyIdDirectionItem);
            }
        }

        private string nameDirectionItem;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpDirection", ViewItemType = ViewItemType.Content)]
        public string NameDirectionItem
        {
            get { return nameDirectionItem; }
            set
            {
                nameDirectionItem = value;
                RaisePropertyChanged(NotifyNameDirectionItem);
            }
        }

        private bool isDirectionLeftRight;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isDirectionLeftRight", ViewItemType = ViewItemType.Other)]
		public bool IsDirectionLeftRight
        {
            get { return isDirectionLeftRight; }
            set
            {

                isDirectionLeftRight = value;

                RaisePropertyChanged(NotifyIsDirectionLeftRight);

            }
        }


        private bool filterNullItem;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "btnNullItem", ViewItemType = ViewItemType.Checked)]
        public bool FilterNullItem
        {
            get { return filterNullItem; }
            set
            {
                filterNullItem = value;
                RaisePropertyChanged(NotifyFilterNullItem);
            }
        }

        private bool isenabledNullObject;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "btnNullItem", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabledNullObject
        {
            get { return isenabledNullObject; }
            set
            {

                isenabledNullObject = value;

                RaisePropertyChanged(NotifyIsEnabledNullObject);

            }
        }

        private bool isenabledRemoveObject;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "btnRemoveObject", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabledRemoveObject
        {
            get { return isenabledRemoveObject; }
            set
            {

                isenabledRemoveObject = value;

                RaisePropertyChanged(NotifyIsEnabledRemoveObject);

            }
        }

        private bool isenabledAddObject;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "btnAddObject", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabledAddObject
        {
            get { return isenabledAddObject; }
            set
            {

                isenabledAddObject = value;

                RaisePropertyChanged(NotifyIsEnabledAddObject);

            }
        }


        private void ValidateButtons()
        {
            IsEnabledAddObject = IsEnabledNullObject = !string.IsNullOrEmpty(IdClassItem) &&
                    !string.IsNullOrEmpty(IdRelationTypeItem);

            IsEnabledRemoveObject = !string.IsNullOrEmpty(IdObjectItem);
        }
    }
}
