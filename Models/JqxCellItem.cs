﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Models
{
    public class JqxCellItem
    {
        public string Id { get; set; }
        public string ColumnName { get; set; }
        public int RowId { get; set; }
        public string Value { get; set; }
    }
}
