﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Models
{
    public class ColumnSortItem
    {
        public string Column { get; set; }
        public string Direction { get; set; }
    }
}
