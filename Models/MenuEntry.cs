﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Models
{
    public class MenuEntry
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public bool IsVisible { get; set; }
        public bool IsEnabled { get; set; }

        public string ParentId
        {
            get
            {
                return ParentEntry == null ? "-1" : ParentEntry.Id;
            }
        }

        public MenuEntry ParentEntry { get; set; }

        public string Html
        {
            get
            {
                var result = "<i class=\"fa " + Icon + "\" aria-hidden=\"true\"></i>&nbsp;";
                result += "<label id=\"" + Id + "\">" + Name + "</label>";
                return result;
            }
        }

        public MenuEntry()
        {
            ParentEntry = null;
        }
    }
}
