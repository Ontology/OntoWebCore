﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Models
{
    public enum NodeColor
    {
        None = 0,
        Green = 1,
        Mediumaquamarine = 2,
        Sandybrown = 3
    }
    public enum NodeType
    {
        AttributeRel = 0,
        NodeForwardFormal = 1,
        NodeBackwardFormal = 2,
        NodeForwardInformal = 3,
        NodeBackwardInformal = 4
    }
    public class ObjectRelNode
    {
        public string IdNode { get; set; }
        private bool isLeftRightNode;
        private bool isInformal;
        public string IdLeft { get; set; }
        public string NameLeft { get; set; }
        public string IdRight { get; set; }
        public string NameRight { get; set; }
        public string IdRelationType { get; set; }
        public string NameRelationType { get; set; }
        public long Min { get; set; }
        public long Count { get; set; }
        public long MaxForw { get; set; }
        public long MaxBackw { get; set; }
        public string Class
        {
            get
            {
                if (Count > 0)
                {
                    return "fa fa-caret-square-o-right";
                }
                else
                {
                    return "";
                }
            }
        }

        public string NodeName
        {
            get
            {
                if (isLeftRightNode)
                {
                    if (isInformal)
                    {
                        return NameRelationType + " (" + Count + ")";
                    }
                    else
                    {
                        return NameRelationType + "/" + NameRight + " (" + Min + "/" + Count + "/" + MaxForw + "/" + MaxBackw + ")";
                    }
                    
                }
                else
                {
                    if (isInformal)
                    {
                        return NameRelationType + "/" + NameLeft + " (" + Count+ ")";
                    }
                    else
                    {
                        return NameRelationType + "/" + NameLeft + " (" + Min + "/" + Count + "/" + MaxForw + "/" + MaxBackw + ")";
                    }
                    
                }
                
            }
        }

        public string Color
        {
            get
            {
                if (Count < Min)
                {
                    return NodeColor.Sandybrown.ToString().ToLower();
                }
                else
                {
                    if (Count > MaxForw && MaxForw > -1)
                    {
                        return NodeColor.Sandybrown.ToString().ToLower();
                    }
                }

                

                return NodeColor.Green.ToString().ToLower();
            }
        }

        public NodeType NodeType { get; set; }

        public string Ontology { get; set; }
        

        public ObjectRelNode(bool isLeftRightNode, bool isInformal)
        {
            this.isLeftRightNode = isLeftRightNode;
            this.isInformal = isInformal;
        }

    }
}
