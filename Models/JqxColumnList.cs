﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Models
{
    public class JqxColumnList
    {
        public List<JqxColumnAttribute> ColumnList { get; set; }
    }
}
