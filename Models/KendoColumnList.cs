﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Models
{
    public class KendoColumnList
    {
        public List<KendoColumnAttribute> ColumnList { get; set; }
    }
}
