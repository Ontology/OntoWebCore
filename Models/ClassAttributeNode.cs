﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Models
{
    

    public class ClassAttributeNode
    {
        public string IdNode { get; set; }
        public string IdClass { get; set; }
        public string NameClass { get; set; }
        public string IdAttributeType { get; set; }
        public string NameAttributeType { get; set; }
        public long Min { get; set; }
        public long Count { get; set; }
        public long Max { get; set; }
        public string Class
        {
            get
            {
                if (Count > 0)
                {
                    return "fa fa-caret-square-o-right";
                }
                else
                {
                    return "";
                }
            }
        }

        public string NodeName
        {
            get
            {
                return NameAttributeType + " (" + Min + "/" + Count + "/" + Max + ")";
            }
        }

        public string Color
        {
            get
            {
                if (Count < Min)
                {
                    return NodeColor.Sandybrown.ToString().ToLower();
                }
                else
                {
                    if (Count > Max && Max > -1)
                    {
                        return NodeColor.Sandybrown.ToString().ToLower();
                    }
                }

                return NodeColor.Green.ToString().ToLower();
            }
        }            
        

    }
}
