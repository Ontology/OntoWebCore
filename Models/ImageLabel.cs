﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Models
{
    public class ImageLabel
    {
        public string FontIcon { get; set; }
        public string Label { get; set; }
    }
}
