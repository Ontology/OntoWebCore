﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Models
{
    public class KendoDatasource
    {
        public KendoTransport transport { get; set; }
        public int? pageSize { get; set; }
    }
}
