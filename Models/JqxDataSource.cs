﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Models
{
    public enum JsonDataType
    {
        Json = 0,
        Jsonp = 1
    }
    public class JqxDataSource
    {
        public string datatype { get; set; }

        public List<DataField> datafields { get; set; }

        public string id { get; set; }

        public int? pagenum { get; set; }

        public int? pagesize { get; set; }

        public object localdata { get; set; }
        public string url { get; set; }
        public bool async { get; set; }

        public JqxDataSource(JsonDataType dataType)
        {
            this.datatype = dataType.ToString().ToLower();
        }
    }
}
