﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Models
{
    public class NavigationNodeChangeItem
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public bool ListenMode { get; set; }
        public string Class { get; set; }
        public string Color { get; set; }
    }
}
