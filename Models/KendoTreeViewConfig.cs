﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Models
{
    public class KendoTreeViewConfig
    {
        public string dataTextField { get; set; }
        public KendoHierarchicalDataSource dataSource { get; set; }
    }

    public class KendoHierarchicalDataSource
    {
        public KendoTransport transport { get; set; }
        public KendoHierarchicalSchema schema { get; set; }
    }    

    public class KendoHierarchicalSchema
    {
        public KendoHierarchicalModel model { get; set; }
    }

    public class KendoHierarchicalModel
    {
        public string id { get; set; }
        public string children { get; set; }
        public string hasChildren { get; set; }
    }

    public class KendoTreeNode
    {
        public string NodeId { get; set; }
        public string NodeUId { get; set; }
        public string NodeName { get; set; }
        public bool HasChildren
        {
            get
            {
                return SubNodes != null && SubNodes.Any();
            }
        }
        public List<KendoTreeNode> SubNodes { get; set; }

    }
    
}
