﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoMsg_Module.Models
{
    public class ViewTreeChangeNode
    {
        public string Id { get; set; }
        public string NewLabel { get; set; }
    }
}
