﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Models
{
    public enum DataFieldType
    {
        String = 0,
        Bool = 1,
        Number = 2,
        Date = 3,
        Float = 4,
        Int = 5
    }
    public class DataField
    {
        public string name { get; set; }
        public string type { get; set; }

        public DataField(DataFieldType dataFieldType)
        {
            type = dataFieldType.ToString().ToLower();
        }
    }
}
