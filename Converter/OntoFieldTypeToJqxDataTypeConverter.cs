﻿using OntoWebCore.Attributes;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Converter
{
    public static class OntoFieldTypeToJqxDataTypeConverter
    {
        public static DataFieldType Convert(string idFieldType)
        {
            if (idFieldType == "b82079193a28401393bf81ce1c81908e")
            {
                return DataFieldType.Bool;
            }
            else if (idFieldType == "e1ccab17b2c54ff78d671e8b0bd51a91")
            {
                return DataFieldType.Number;
            }
            else if (idFieldType == "6e778f6642b94422acbf0d6be476efea")
            {
                return DataFieldType.Int;
            }
            else if (idFieldType == "38090631b0964c3fbe03cf79d6a537ae")
            {
                return DataFieldType.Date;
            }
            else 
            {
                return DataFieldType.String;
            }
        }
    }
}
