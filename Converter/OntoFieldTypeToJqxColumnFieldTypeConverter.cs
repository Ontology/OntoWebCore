﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Converter
{
    public static class OntoFieldTypeToJqxColumnFieldTypeConverter
    {
        public static ColumnType Convert(string idFieldType)
        {
            if (idFieldType == "b82079193a28401393bf81ce1c81908e")
            {
                return ColumnType.Checkbox;
            }
            else
            {
                return ColumnType.Textbox;
            }
        }
    }
}
