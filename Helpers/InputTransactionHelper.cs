﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace OntoWebCore.Helpers
{
    public delegate void SaveInput(string value);
    public delegate void SavedInput(clsOntologyItem result);

    public enum SaveType
    {
        Attribute = 0,
        ObjectRelationWithClass = 1,
        ObjectRelation = 2,
        Object = 3
    }
    public class InputTransactionHelper : NotifyPropertyChange
    {
        private Timer timerInput;

        private clsOntologyItem resultSave;

        private Globals globals;
        private clsOntologyItem oItemObject;
        private clsOntologyItem oItemAttributeType;
        private clsOntologyItem oItemClassRelation;
        private clsOntologyItem oItemRelatedObject;
        private clsOntologyItem oItemRelationType;
        private bool order;
        private bool removeAll;
        private bool saveAutomatic;

        private clsTransaction transaction;
        private clsRelationConfig relationConfig;

        private OntologyModDBConnector dbReader;

        private SaveType saveType;

        private string inputValue;
        public event SaveInput saveInput;
        public event SavedInput savedInput;

        public string Value
        {
            get { return inputValue; }
            set
            {
                inputValue = value;
                timerInput.Stop();
                timerInput.Start();
               
                RaisePropertyChanged(nameof(Value));
            }
        }

        private void TimerInput_Elapsed(object sender, ElapsedEventArgs e)
        {
            timerInput.Stop();
            saveInput?.Invoke(Value);
            if (saveAutomatic)
            {
                if (saveType == SaveType.Attribute)
                {
                    var relToSave = relationConfig.Rel_ObjectAttribute(oItemObject, oItemAttributeType, Value, getNextOrderId: order);

                    transaction.ClearItems();
                    var result = transaction.do_Transaction(relToSave, boolRemoveAll: removeAll);

                    savedInput?.Invoke(result);
                }
                else if (saveType == SaveType.ObjectRelationWithClass)
                {
                    if (!string.IsNullOrEmpty(Value))
                    {
                        var result = dbReader.GetDataObjects(new List<clsOntologyItem>
                        {
                            new clsOntologyItem { Name = Value, GUID_Parent = oItemClassRelation.GUID }
                        });

                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            transaction.ClearItems();
                            var name = GetShortName();
                            var oItemRelatated = new clsOntologyItem
                            {
                                GUID = globals.NewGUID,
                                Name = name,
                                GUID_Parent = oItemClassRelation.GUID,
                                Type = globals.Type_Object
                            };

                            result = transaction.do_Transaction(oItemRelatedObject);
                            if (result.GUID == globals.LState_Success.GUID)
                            {
                                var relToSave = relationConfig.Rel_ObjectRelation(oItemObject, oItemRelatedObject, oItemRelationType, getNextOrderId: order);

                                result = transaction.do_Transaction(relToSave, boolRemoveAll: removeAll);

                                if (result.GUID == globals.LState_Error.GUID)
                                {
                                    transaction.rollback();
                                }
                            }
                        }

                        savedInput?.Invoke(result);
                    }
                }
                else if (saveType == SaveType.ObjectRelation)
                {
                    var name = GetShortName();
                    oItemRelatedObject.Name = name;
                    transaction.ClearItems();
                    var relToSave = relationConfig.Rel_ObjectRelation(oItemObject, oItemRelatedObject, oItemRelationType, getNextOrderId: order);

                    var result = transaction.do_Transaction(relToSave, boolRemoveAll: removeAll);

                    savedInput?.Invoke(result);
                }
                else if (saveType == SaveType.Object)
                {
                    var name = GetShortName();
                    oItemObject.Name = name;
                    transaction.ClearItems();

                    var result = transaction.do_Transaction(oItemObject);

                    savedInput?.Invoke(result);
                }
            }

        }

        private string GetShortName()
        {
            var name = Value;

            if (name.Length > 255)
            {
                name = name.Substring(0, 254);
            }

            return name;
        }

        /// <summary>
        /// Saves the attribute as the name of the object
        /// </summary>
        /// <param name="globals">For the connection to ElasticSearch</param>
        /// <param name="oItemObject">The object which which name will be the attribute</param>
        /// <param name="saveAutomatic">Save the item automatically</param>
        public InputTransactionHelper(Globals globals,
            clsOntologyItem oItemObject,
            bool saveAutomatic)
        {
            this.globals = globals;
            this.oItemObject = oItemObject;
            this.saveAutomatic = saveAutomatic;
            saveType = SaveType.Object;

            Initialize();
        }

        /// <summary>
        /// Saves the attribute as an attribute-relatione
        /// </summary>
        /// <param name="globals">For the connection to ElasticSearch</param>
        /// <param name="oItemObject">The object which will be extended by the attribute</param>
        /// <param name="oItemAttributeType">The attributetype of the value</param>
        /// <param name="order">Take the next order-id</param>
        /// <param name="removeAll">Remove all other attributes</param>
        /// <param name="saveAutomatic">Save the item automatically</param>
        public InputTransactionHelper(Globals globals, 
            clsOntologyItem oItemObject, 
            clsOntologyItem oItemAttributeType, 
            bool order,
            bool removeAll,
            bool saveAutomatic)
        {
            this.globals = globals;
            this.oItemObject = oItemObject;
            this.oItemAttributeType = oItemAttributeType;
            this.order = order;
            this.removeAll = removeAll;
            this.saveAutomatic = saveAutomatic;
            saveType = SaveType.Attribute;

            Initialize();
        }

        /// <summary>
        /// Saves the attribute as an object-relation
        /// </summary>
        /// <param name="globals">For the connection to ElasticSearch</param>
        /// <param name="oItemObject">The object which will be extended by the attribute</param>
        /// <param name="oItemClassRelation">The class of the object which will take the role of an attribute</param>
        /// <param name="oItemRelationType">The relationtype to relate the two objects</param>
        /// <param name="order">Take the next order-id</param>
        /// <param name="removeAll">Remove all other attributes</param>
        /// <param name="searchExisting">Search an existing object for relation</param>
        /// <param name="saveAutomatic">Do an automatic save of related object</param>
        public InputTransactionHelper(Globals globals,
            clsOntologyItem oItemObject,
            clsOntologyItem oItemClassRelation,
            clsOntologyItem oItemRelationType,
            bool order,
            bool removeAll,
            bool searchExisting,
            bool saveAutomatic)
        {
            this.globals = globals;
            this.oItemObject = oItemObject;
            this.oItemClassRelation = oItemClassRelation;
            this.oItemRelationType = oItemRelationType;
            this.order = order;
            this.removeAll = removeAll;
            this.saveAutomatic = saveAutomatic;
            saveType = SaveType.ObjectRelationWithClass;

            Initialize();
        }

        /// <summary>
        /// Saves the attribute as an object-relation
        /// </summary>
        /// <param name="globals">For the connection to ElasticSearch</param>
        /// <param name="oItemObject">The object which will be extended by the attribute</param>
        /// <param name="oItemRelatedObject">The object to be related as an attribute</param>
        /// <param name="oItemRelationType">The relationtype to relate the two objects</param>
        /// <param name="order">Take the next order-id</param>
        /// <param name="removeAll">Remove all other attributes</param>
        /// <param name="saveAutomatic">Search an existing object for relation</param>
        public InputTransactionHelper(Globals globals,
            clsOntologyItem oItemObject,
            clsOntologyItem oItemRelatedObject,
            clsOntologyItem oItemRelationType,
            bool order,
            bool removeAll,
            bool saveAutomatic)
        {
            this.globals = globals;
            this.oItemObject = oItemObject;
            this.oItemRelatedObject = oItemRelatedObject;
            this.oItemRelationType = oItemRelationType;
            this.order = order;
            this.removeAll = removeAll;
            this.saveAutomatic = saveAutomatic;
            saveType = SaveType.ObjectRelation;

            Initialize();
        }

        private void Initialize()
        {
            transaction = new clsTransaction(globals);
            timerInput = new Timer();
            timerInput.Interval = 300;
            timerInput.Elapsed += TimerInput_Elapsed;

            dbReader = new OntologyModDBConnector(globals);
            transaction = new clsTransaction(globals);
            relationConfig = new clsRelationConfig(globals);
        }

    }
}
