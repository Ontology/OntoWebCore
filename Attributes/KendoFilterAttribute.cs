﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Attributes
{
    public class KendoFilterAttribute : Attribute
    {
        public bool extra { get; set; }
    }
}
