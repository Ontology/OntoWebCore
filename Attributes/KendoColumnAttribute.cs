﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Attributes
{
    public class KendoColumnAttribute : Attribute
    {
        public string title { get; set; }
        public string field { get; set; }
        public bool hidden { get; set; }
        public string format { get; set; }
        public string width { get; set; }
        public int Order { get; set; }
        public bool filterable { get; set; }
        public bool editable { get; set; }
        public string type { get; set; }
        public string template { get; set; }
        public string[] aggregates { get; set; }
        public string footerTemplate { get; set; }
    }
}
