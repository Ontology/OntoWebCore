﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Attributes
{
    public class KendoDataSourceFieldAttribute : Attribute
    {
        public string type { get; set; }
        public bool nullable { get; set; }
        public string field { get; set; }
        public bool IsIdField { get; set; }
        public bool IsParentIdField { get; set; }
    }
}
