﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Attributes
{
    public class KendoGridConfigAttribute : Attribute
    {
        public string height { get; set; }
        public string width { get; set; }
        public bool groupbable { get; set; }
        public bool sortable { get; set; }
        public bool autoBind { get; set; }
        public string selectable { get; set; }
        public string rowTemplate { get; set; }
        public string editable { get; set; }
    }
}
