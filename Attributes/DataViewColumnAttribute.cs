﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Attributes
{
    public enum CellType
    {
        String = 0,
        Integer = 1,
        Number = 2,
        Date = 4,
        Boolean = 8,
        Url = 16
    }
    public enum StandardRenderer
    {
        Generic = 0,
        linkrenderer = 1
    }
    public class DataViewColumnAttribute : Attribute
    {
        public bool IsIdField { get; set; }
        public bool IsVisible { get; set; }
        public bool IsReadonly { get; set; }
        public int DisplayOrder { get; set; }
        public string Caption { get; set; }
        public object Width { get; set; }
        public CellType CellType { get; set; }
        public string CellsFormat { get; set; }
        public StandardRenderer StandardRenderer {get; set;}

    }
}
