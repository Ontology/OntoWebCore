﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Attributes
{
    public class ComboboxAttribute : Attribute
    {
        public bool IsIdField { get; set; }
        public string Property { get; set; }
        public CellType FieldType { get; set; }
    }
}
