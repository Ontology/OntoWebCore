﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Attributes
{
    public class KendoPageableAttribute : Attribute
    {
        public bool refresh { get; set; }
        public int[] pageSizes { get; set; }
        public int buttonCount { get; set; }
        public int pageSize { get; set; }
    }
}
