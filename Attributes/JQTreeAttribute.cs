﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Attributes
{
    public class JQTreeAttribute : Attribute
    {
        public string DataAttribute { get; set; }
        public bool SubNodes { get; set; }
    }
}
