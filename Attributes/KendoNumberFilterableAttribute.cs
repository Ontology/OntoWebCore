﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Attributes
{
    public class KendoNumberFilterableAttribute : Attribute
    {
        public string contains { get; set; }
        public string startsWith { get; set; }
        public string eq { get; set; }
        public string neq { get; set; }
        public string isnull { get; set; }
        public string isnotnull { get; set; }
    }
}
