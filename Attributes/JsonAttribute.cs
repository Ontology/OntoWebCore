﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Attributes
{
    public class JsonAttribute : Attribute
    {
        public string Property { get; set; }
        public bool PropertyNameToLowercase { get; set; }
        public bool EnumToString { get; set; }
        public bool EnumStringToLowercase { get; set; }
        public bool EnumStringFirstCharToLowercase { get; set; }
        public bool FirstCharLowercase { get; set; }
    }
}
