﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Attributes
{
    public class KendoTreeListConfigAttribute : Attribute
    {
        public bool expanded { get; set; }
        public string width { get; set; }
        public string height { get; set; }
        public bool filterable { get; set; }
        public bool sortable { get; set; }
    }
}
