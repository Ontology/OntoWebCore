﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoWebCore.Attributes
{
    public enum AlignType
    {
        Left = 0,
        Right = 1
    }
    public enum ColumnType
    {
        None = 0,
        Textbox = 1,
        Checkbox = 2
    }
    public class JqxColumnAttribute : Attribute
    {
        public string text { get; set; }
        public string columngroup { get; set; }
        public string datafield { get; set; }
        public bool hidden { get; set; }
        public string cellsalign { get; private set; }
        public string align { get; private set; }
        public string cellsformat { get; set; }
        public string columntype { get; private set; }
        public bool editable { get; set; }
        public object width { get; set; }
        public bool filterable { get; set; }
        public string cellsrenderer { get; set; }

        public JqxColumnAttribute(AlignType cellsAlign, AlignType align, ColumnType columnType)
        {
            this.cellsalign = cellsAlign.ToString().ToLower();
            this.align = align.ToString().ToLower();
            if (columnType != ColumnType.None)
            {
                this.columntype = columnType.ToString().ToLower();
            }
            
            this.filterable = true;
        }

    }
}
